<?php

/**
 * @file
 *   Contains additional hooks and helpers for the Mode module.
 */

/**
 * Administrative settings form.
 */
function modes_admin($form_state) {
  $form = array();
  $options = array('' => t('None'));
  foreach (features_get_features(NULL, TRUE) as $name => $details) {
    if (isset($details->info['features']['modes'])) {
      $options[$name] = $details->info['name'];
    }
  }
  if (count($options) > 1) {
    $form['modes_mode'] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => variable_get('modes_mode', ''),
    );
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  else {
    $form['info'] = array(
      '#value' => t('No modes are available. You may need to refresh your cache.'),
    );
  }
  return $form;
}

/**
 * Submit callback for the administrative settings form.
 */
function modes_admin_submit($form, $form_state) {
  if (modes_set($form_state['values']['modes_mode'])) {
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 * Implementation of hook_features_export().
 */
function modes_features_export($data, &$export, $module_name) {
  $export['features']['modes']['modes'] = $module_name;
  $export['hidden'] = 'true';
  $export['dependencies']['features'] = 'features';
  return array();
}

/**
 * Implementation of hook_features_export_options().
 */
function modes_features_export_options() {
  return array(
    'modes' => t('This feature is a mode'),
  );
}

/**
 * Implementation of hook_features_export_render().
 */
function modes_features_export_render($module_name, $data, $export = NULL) {
  return array('modes_defaults' => '  $mode = TRUE;');
}
