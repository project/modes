Modes is a module that provides exchangeable site configurations. Each Mode is
really a Feature module -- a bundle of settings and exportables.

After installing the Mode module, you will have a new option when creating
Features called "This feature is a mode" in the "Modes" group. Selecting this
option will make your Feature into a Mode.

To enable your Mode after uploading it to your site, select it from the list of
Modes at admin/settings/modes or call modes_set($mode_name) from code. (Use
modes_get() to get the currently enabled Mode in code.) Only one Mode can be
enabled at a time.

Enabling a Mode will do two things: it will enable the associated feature (and
disable the feature associated with the previously selected Mode, if
applicable); and it will add a class to your site's <body> tag if your theme
uses the $body_classes variable in page.tpl.php. This class allows you to theme
your site differently depending on which Mode is enabled.

This module was developed by Isaac Sukin (IceCreamYou) and sponsored by Acquia
for use in Drupal Commons.